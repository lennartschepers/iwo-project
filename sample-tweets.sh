#!/bin/bash
# name: sample-tweets

echo this hour had $(zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | wc -l) tweets

echo this hour had $(zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | sort | uniq -u | wc -l) unique tweets

echo this hour had $(zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | sort | uniq -u | grep "RT" | wc -l) Retweets

zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | grep -v "RT" |  sort | uniq -u | head -20 | fmt


