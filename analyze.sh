#!/bin/bash 
# Name: analyze.sh 
# This script analyzes all tweets from the corpus from 01-11-2016 untill 17-11-2016 for cursewords from a list and outputs 
# the total amount of found curse words per day in our results.txt file. Reasoning for the time window is that the election 
# results became known in the early morning of 08-11-2016, so the examined period is about the same before as after the 
# occurence. 
# Date: 20-03 # author: Lennart Schepers
for i in {01..17} ;
do echo ${i} november ; #repeats the day for ease of documentation
echo $(zgrep -oFwf ~/iwo-project/scheldwoorden.txt /net/corpora/twitter2/Tweets/2016/11/201611$i:*.out.gz |wc -l ); 
	#zgrep allows me to grep without extracting the tar.gz files. "-o" only shows the part of a matching line that matches the pattern. "-F" interprets pattern as a list of fixed strings, any of which is to be matched. "-f" obtain pattern from file" "-w" selects only whole words.

#at the end of the path, $i reflects the number in the for loop, while the asterisk signifies that every hour in the day is accumulated.
done

